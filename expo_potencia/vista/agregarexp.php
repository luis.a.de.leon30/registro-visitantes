<?php 


require_once ("header.php");
?>


<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper1">  
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <center>
              <img src="../public/images/LOGO_PETRO-CONTAINER.png" width="20%">
              
              </center>
              <div class="col-md-12">

                  <div class="box">
                    <div class="box-header with-border">
                          <h1 class="box-title">Agregar Visitantes </h1>
                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <!-- centro -->
                    
                    <div class="panel-body"  id="formularioregistros">
                      <form action="formulario" id="formulario" method="POST">


                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                          <label>Nombres </label>
                          <input type="text" class="form-control" name="nombres" id="nombres" maxlength="256" placeholder="Nombres" required>
                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                          <label>Apellidos </label>
                          <input type="text" class="form-control" name="apellidos" id="apellidos" maxlength="256" placeholder="Apellidos" required>
                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                          <label>Cedula del invitado: </label>
                          <input type="text" class="form-control" name="cedula" id="cedula" maxlength="11" placeholder="Cedula" required>
                        </div>

                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                          <label>Correo Electronico </label>
                          <input type="text" class="form-control" name="email" id="email" maxlength="256" placeholder="Correo Electronico">
                        </div>

                         <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                          <label>Ente </label>
                          <input type="text" class="form-control" name="ente" id="ente" maxlength="256" placeholder="Ente al que pertenece">
                        </div>
                        
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                          <!--<button class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>-->
                          <button class="btn btn-success" type="submit" id="btnGuardar"><i class="fa fa-plus-circle"></i> Agregar</button> 
                          <a href="reporte.php" target="_blank"><button class="btn btn-danger" type="submit" id=""><i class="fa fa-plus-circle"></i> Consultar</button></a> 



                        </div>

                      </form>
                    </div>
                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
<?php 

require_once 'footer.php';


?>

<script type="text/javascript" src="scripts/agregarexp.js"></script>
<?php 
ob_end_flush();
 ?>
