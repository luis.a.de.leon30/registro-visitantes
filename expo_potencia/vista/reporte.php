<?php 
require_once "header.php";
?>


<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper1">  
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <center>
              <img src="../public/images/LOGO_PETRO-CONTAINER.png" width="20%">
              
              </center>
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border">
                          <h1 class="box-title">Editar Empleado </h1>
                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body table-responsive" id="listadoregistros">
                        <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">
                          <thead>
                            <th>ID</th>
                            <th>Cedula</th>
                            <th>Nombres</th>
                            <th>Apellidos</th>
                            <th>Email</th>
                            <th>Ente</th>
                            
                            <!--descomentar cuando se tenga la informacion de los empleado activos<th>Estado</th>-->
                          </thead>
                          <tbody>
                          </tbody>
                          
                        </table>
                    </div>

                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
<?php 

require_once 'footer.php';

?>

<script type="text/javascript" src="scripts/reporte.js"></script>
<script type="text/javascript" src='../public/datatables/jquery.dataTables.min.js'></script>
<script type="text/javascript" src='../public/datatables/pdfmake.min.js'></script>
<script type="text/javascript" src='../public/datatables/dataTables.buttons.min.js'></script>
<script type="text/javascript" src='../public/datatables/buttons.dataTables.min.css'></script>
<script type="text/javascript" src='../public/datatables/jquery.dataTables.min.css'></script>
<script type="text/javascript" src='../public/datatables/buttons.html5.min.js'></script>

<?php 
ob_end_flush();
 ?>
