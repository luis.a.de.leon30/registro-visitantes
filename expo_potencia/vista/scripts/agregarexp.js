
function init()
{
	$("#formulario").on("submit",function(e){
		registrar(e);
	})
}

//funcion limpiar
function limpiar()
{
	$("#cedula").val("");
	$("#nombres").val("");
	$("#apellidos").val("");
	$("#email").val("");
}

//funcion para guardar la asistencia
function registrar(e)
{
	e.preventDefault(); //No se activará la acción predeterminada del evento
	$("#btnGuardar").prop("disabled",false);
	var formData = new FormData($("#formulario")[0]);
	$.ajax({
		url: "../ajax/invitado.php?op=agregarexp",
	    type: "POST",
	    data: formData,
	    contentType: false,
	    processData: false,

	    success: function(datos)
	    {     
	         	bootbox.alert(datos);
	    }
	});
	limpiar()
}

init();