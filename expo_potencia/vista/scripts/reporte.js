var tabla;

function init()
{
	listar();
}

function listar()
{
	tabla=$('#tbllistado').dataTable(
		{ 
			"aProcessing": true,//Activamos el procesamiento del datatables
		    "aServerSide": true,//Paginación y filtrado realizados por el servidor
		    dom: 'Bfrtip',//Definimos los elementos del control de tabla
		    buttons: [
						
					],
			"ajax":
					{
						url: '../ajax/invitado.php?op=reporte',
						type: "get",
						datatype: "json",
						error:function(e){
							console.log(e.responseText);
						}
					},
			"bDestroy": true,
			"iDisplayLength": 10,//paginacion cada diez registro
			"order": [[0 , "desc"]]//ordenar (columna,orden)
		}
	).DataTable();
}



init();