


<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Registro de Visitantes</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../public/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../public/css/font-awesome.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../public/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../public/css/_all-skins.min.css">
    <link rel="apple-touch-icon" href="../public/img/apple-touch-icon.png">
    <link rel="shortcut icon" href="../public/img/favicon.ico">

    <link rel="stylesheet" type="text/css" href="../public/css/bootstrap-select.min.css">


  </head>
  <body class="hold-transition skin-red-light sidebar-mini">
    <div class="wrapper">

      <header class="main-header skin-red-light">

        <!-- Logo -->
        <a href="#" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>Petro Container</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Registro de Visitantes a la Expo Potencia Venezuela</b></span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Navegación</span>
          </a>
          <!-- Navbar Right Menu -->

        </nav>
      </header>
      
      <!-- Left side column. contains the logo and sidebar -->
      <!--<aside class="main-sidebar">-->
        <!-- sidebar: style can be found in sidebar.less -->
        <!--<section class="sidebar">  -->     
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <!--<ul class="sidebar-menu">
            <li class="header"></li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-laptop"></i>
                <span>Empleados</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="asistencia.php"><i class="fa fa-circle-o"></i>Ingresar asistencia</a></li>
                <li><a href="agregaremp.php"><i class="fa fa-circle-o"></i>Agregar empleado</a></li>
                <li><a href="editaremp.php"><i class="fa fa-circle-o"></i>Editar empleado</a></li>
              </ul>
            </li>

            <li class="treeview">
              <a href="#">
                <i class="fa fa-bar-chart"></i>
                <span>Consultas</span>
                 <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="asistenciafecha.php"><i class="fa fa-circle-o"></i>Consultar asistencia por fecha</a></li>
              </ul>
            </li>

            <li class="treeview">
              <a href="#">
                <i class="fa fa-th"></i>
                <span>Usuarios</span>
                 <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="cambiopswrd.php"><i class="fa fa-circle-o"></i>Cambiar contraseña</a></li>
                <li><a href="agregarusr.php"><i class="fa fa-circle-o"></i>Agregar usuarios</a></li>
                <li><a href="editarusr.php"><i class="fa fa-circle-o"></i>Editar usuarios</a></li>
              </ul>
            </li>                        
          </ul>
        </section>-->
        <!-- /.sidebar -->
        
      </aside>

