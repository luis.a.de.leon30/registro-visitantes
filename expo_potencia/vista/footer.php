    <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0
        </div>
        <strong>Gerencia de Sistema - Industria Canaima.</strong><br>
        <strong>Desarrollado por <a>Crhistian Mendoza</a>.</strong>
    </footer>  
    <!-- jQuery -->
    <script src="../public/js/jquery-3.1.1.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../public/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../public/js/app.min.js"></script>

    <!--bootbox-->
    <script src="../public/js/bootbox.min.js"></script>
    <script src="../public/js/bootstrap-select.min.js">    </script>
    <!--<script>
    window.onload = function(){killerSession();}
    function killerSession(){
    setTimeout("window.open('../ajax/usuario.php?op=salir','_top');",300000);
    }
    </script>-->
  </body>
</html>
