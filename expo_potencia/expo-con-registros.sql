-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 23, 2018 at 01:33 PM
-- Server version: 10.0.32-MariaDB-0+deb8u1
-- PHP Version: 7.0.29-1~dotdeb+8.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `expo`
--

-- --------------------------------------------------------

--
-- Table structure for table `asistencia_expo`
--

CREATE TABLE IF NOT EXISTS `asistencia_expo` (
`id` int(4) NOT NULL,
  `cedula` int(8) NOT NULL,
  `nombres` varchar(256) NOT NULL,
  `apellidos` varchar(256) NOT NULL,
  `email` varchar(256) DEFAULT NULL,
  `ente` varchar(256) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asistencia_expo`
--

INSERT INTO `asistencia_expo` (`id`, `cedula`, `nombres`, `apellidos`, `email`, `ente`) VALUES
(1, 20096874, 'Keily Mar', 'Quitana Quintana', 'keily2904.qq@gmail.com', 'Fundabit'),
(2, 24757924, 'luis', 'de leon', 'luis.a.de.leon30@gmail.com', 'industria canaima'),
(3, 23693974, 'luis', 'de leon hernandez', 'luis.a.de.leon30@gmail.com', 'infocentro'),
(4, 12345678, 'Jean ', 'Castro', 'jeanmcastro@gmail.com', 'IC'),
(5, 23693974, 'Eladio', 'De León', 'admin@admin.com', 'Fundabit'),
(6, 11244545, 'Cristian', 'Mendoza', 'hola', 'sdsdsd');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `asistencia_expo`
--
ALTER TABLE `asistencia_expo`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `asistencia_expo`
--
ALTER TABLE `asistencia_expo`
MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
